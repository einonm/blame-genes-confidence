# How to blame your genes with confidence

JupyterNB slides for a talk @SWLUG

The source is available in the gh-pages branch of this repository.

The slides can be viewed at:

http://einon.net/blame-genes-confidence
